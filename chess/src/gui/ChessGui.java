package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


import board.Board;
import board.Cell;
import player.Player.id;
import board.Coordinate;
import pieces.Piece;

// Source citing: got tips from https://www.youtube.com/watch?v=w9HR4VJ8DAw

public class ChessGui{
	
	private final JFrame frame; 
	private final GuiBoard boardFrame; 
	private final Board board; 
	
	private Cell sourceCoordinate;
	private Cell destinationCoordinate; 
	private Piece movingPiece;
	
	private final static Dimension BOARD_DIMENSION = new Dimension(400,400);
	private final static Dimension CELL_DIMENSION = new Dimension(10,10);
	private final static String PIECE_IMG_PATH = "PieceIMG/";
	
	public ChessGui(Board board){	// contains board and cells 
		this.board = board; 
		this.frame = new JFrame("Chess");
		this.frame.setLayout(new BorderLayout());
		this.frame.setSize(new Dimension(600,600));
		final JMenuBar tableMenu = new JMenuBar();
		populateMenu(tableMenu);
		this.frame.setJMenuBar(tableMenu);
		this.boardFrame = new GuiBoard(); 
		this.frame.add(this.boardFrame,BorderLayout.CENTER);
		this.frame.setVisible(true);	
	}
	
	
    private JMenu createFile() {
	    	final JMenu fileMenu = new JMenu("File");
	    	
	    	final JMenuItem exitMenuItem = new JMenuItem("exit");
	    	
	    	exitMenuItem.addActionListener(new ActionListener() {
	    		@Override
	    		public void actionPerformed(ActionEvent e) {
	    			System.exit(0);
	    		}
	    	});
	    	fileMenu.add(exitMenuItem);
	    	return fileMenu; 
	}
    
	public static void main(String[] args) {
		Board board = new Board(); 
        board.initializeBoard();
        new ChessGui(board);				//start the gui 
    }
	
	private void populateMenu(final JMenuBar tableMenu) {
		tableMenu.add(createFile());
		// add more menus 
	}
	
	private class GuiBoard extends JPanel{
		final List<GuiCell> cells; 
		
		GuiBoard(){
				super(new GridLayout(8,8));		
				setPreferredSize(BOARD_DIMENSION);   // set board size
				this.cells = new ArrayList<>();
				Color blackColor = Color.decode("#71A8D1");
				Color whiteColor = Color.decode("#F7F9FB");
				for(int i = 0; i< 64; i++) {
					final GuiCell cell = new GuiCell(this,i);
					this.cells.add(cell); 
		            if ((i/8+i%8) % 2 == 0) {
		                cells.get(i).setBackground(whiteColor);	//loop through all the cells and set colors
		            } else {
		                cells.get(i).setBackground(blackColor);
		            } 
					add(cell);
				}
				validate();
			
		}

		public void updateGuiBoard(Board board) {
			// TODO Auto-generated method stub
			removeAll(); 
			for(GuiCell cell : cells) {
				cell.updateCell(board);
				add(cell);
			}
			validate();
			repaint(); 
		}
	}
	
	private class GuiCell extends JPanel {
		private final int cellCode; 
		int xCoord;
		int yCoord;
		GuiCell(final GuiBoard guiboard, final int cellCode){
			super(new GridBagLayout());
			setSize(CELL_DIMENSION);
			this.cellCode = cellCode;  // code calculated by row and column
			assignCellPiece();   // if piece exists on board, assign the piece on the board. 
			validate(); 
			xCoord = cellCode/8;
			yCoord = cellCode%8;
			
			
			addMouseListener(new MouseListener(){

				public void mouseClicked(final MouseEvent e) {
					System.out.println("hi0");
					if(SwingUtilities.isLeftMouseButton(e)) {
						if(sourceCoordinate == null) {
							System.out.println(xCoord);
							System.out.println(yCoord);
							sourceCoordinate = board.getCell(xCoord, yCoord);  //from coord
							movingPiece = sourceCoordinate.getPieceOnCell();
							if(movingPiece == null) sourceCoordinate = null;
							System.out.println(sourceCoordinate.getPieceOnCell());
							System.out.println("hi2");
						}
						else {
							destinationCoordinate = board.getCell(xCoord, yCoord);
							board.makeMove(sourceCoordinate.getPieceOnCell().getCoord(), destinationCoordinate.getPieceOnCell().getCoord()); //moveto Coord
							System.out.println(sourceCoordinate.getPieceOnCell().getCoord());
							System.out.println(destinationCoordinate.getPieceOnCell().getCoord());
						}	
						
					}
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							guiboard.updateGuiBoard(board);	
						}
					});
				
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}			
			
			});
		}
		
		public void updateCell(Board board) {
			// TODO Auto-generated method stub
			assignCellPiece();
            if ((xCoord+yCoord) % 2 == 0) {
                this.setBackground(Color.decode("#F7F9FB"));	//loop through all the cells and set colors
            } else {
                this.setBackground(Color.decode("#71A8D1"));
            } 
    		validate();
    		repaint();
		}


		private void assignCellPiece() {
			this.removeAll();
			int xCoord = cellCode/8;		 // calculate x coordinate 
			int yCoord = cellCode%8;     // calculate y coordinate
			if(board.getCell(xCoord,yCoord).getPieceOnCell() != null) {
				try{ 
					final BufferedImage img = ImageIO.read(new File(PIECE_IMG_PATH + board.getCell(xCoord,yCoord).toString().substring(0,3)+".png"));
					add(new JLabel(new ImageIcon(img)));
				} catch (IOException e) {
					e.getMessage();
				}
				
			}
			
		}
	}
}