package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import board.Board;
import board.Coordinate;
import pieces.Piece;
import player.Player;
import player.Player.id;

public class ChameleonTest {
	Board board;
	Player playerA;
	Player playerB;
	
	@Before
	public void setup() {
        board = new Board(); 
        playerA = board.getPlayer(id.WHITE); 
        playerA = board.getPlayer(id.BLACK); 
        board.initializeBoard();
	}
	
	@Test
	/**
	 * Tests if the piece can move like a rook.
	 */
	public void validMovementTest() {
        board.makeMove(new Coordinate(1,0), new Coordinate(3,0));
        board.makeMove(new Coordinate(6,4), new Coordinate(5,4));
        board.makeMove(new Coordinate(3,0), new Coordinate(5,0));
        board.makeMove(new Coordinate(5,4), new Coordinate(4,4));
        
        board.makeMove(new Coordinate(5,0), new Coordinate(6,1));
        assertEquals(board.getCell(1, 0).getPieceOnCell(),null);
        assertEquals(board.getCell(6, 1).getPieceOnCell().getName(),"Pawn");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("6,1").getName(),"Pawn");
        
        board.makeMove(new Coordinate(4,4), new Coordinate(3,4));
        board.makeMove(new Coordinate(6,1), new Coordinate(7,2));
        assertEquals(board.getCell(6, 1).getPieceOnCell(),null);
        assertEquals(board.getCell(7, 2).getPieceOnCell().getName(),"Bishop");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("7,2").getName(),"Bishop");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("7,2") instanceof pieces.Chameleon, true);
        

	}
}
