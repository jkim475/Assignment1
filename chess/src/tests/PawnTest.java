package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import board.Board;
import board.Coordinate;
import player.Player;
import player.Player.id;


/**
 * Pawn -- One of the piece objects that can move as a Pawn in chess. 
 * 
 * The bishop can move any number of squares diagonally, but may not leap over other pieces.
 *  
 * @author jkim475
 */

public class PawnTest {

	Board board;
	Player playerA;
	Player playerB;
	
	@Before
	public void setup() {
        board = new Board(); 
        playerA = board.getPlayer(id.WHITE); 
        playerA = board.getPlayer(id.BLACK); 
        board.initializeBoard();
	}
	
	@Test
	/**
	 * Tests if the piece can move like a bishop
	 */
	public void validMovementTest() {
        board.makeMove(new Coordinate(1,3), new Coordinate(2,3));
        assertEquals(board.getCell(1,3).getPieceOnCell(),null);
        assertEquals(board.getCell(2,3).getPieceOnCell().getName(),"Pawn");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("2,3").getName(),"Pawn");
        
       
        board.makeMove(new Coordinate(6,1), new Coordinate(4,1));
        assertEquals(board.getCell(6, 1).getPieceOnCell(),null);
        assertEquals(board.getCell(4, 1).getPieceOnCell().getName(),"Pawn");
        assertEquals(board.getPlayer(id.BLACK).getPlayerPieces().get("4,1").getName(),"Pawn");
        
	}
	
	@Test
	/**
	 * Tests if the piece can move like other pieces 
	 */
	public void invalidMovementTest() {
        board.makeMove(new Coordinate(1,3), new Coordinate(2,2));
        board.makeMove(new Coordinate(1,3), new Coordinate(2,4));
        assertEquals(board.getCell(2,2).getPieceOnCell(),null);
        assertEquals(board.getCell(2,4).getPieceOnCell(),null);
        assertEquals(board.getCell(1,3).getPieceOnCell().getName(),"Pawn");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("1,3").getName(),"Pawn");
	}
	
	@Test
	/**
	 * Tests if piece moves out of bounds. 
	 */
	public void outOfBoundTest() {
		
        board.makeMove(new Coordinate(1,3), new Coordinate(3,3));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(3,3), new Coordinate(4,3));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        board.makeMove(new Coordinate(4,3), new Coordinate(5,3));
        board.makeMove(new Coordinate(4,1), new Coordinate(3,1));
        board.makeMove(new Coordinate(5,3), new Coordinate(6,2));
        board.makeMove(new Coordinate(3,1), new Coordinate(2,1));
        board.makeMove(new Coordinate(6,2), new Coordinate(7,1));
        board.makeMove(new Coordinate(2,1), new Coordinate(1,2));
        board.makeMove(new Coordinate(7,1), new Coordinate(8,1));
       
        assertEquals(board.getCell(8, 1).getPieceOnCell(),null);
        assertEquals(board.getCell(7, 1).getPieceOnCell().getName(),"Pawn");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("7,1").getName(),"Pawn");
        

	}
	
	@Test
	/**
	 * Tests if board cell is set to null and other player loses a piece
	 */
	public void captureTest() {
        board.makeMove(new Coordinate(1,3), new Coordinate(3,3));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(3,3), new Coordinate(4,3));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        board.makeMove(new Coordinate(4,3), new Coordinate(5,3));
        board.makeMove(new Coordinate(4,1), new Coordinate(3,1));
        board.makeMove(new Coordinate(5,3), new Coordinate(6,2));
        assertEquals(board.getCell(6, 2).getPieceOnCell().getName(),"Pawn");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("6,2").getName(), "Pawn");
        assertEquals(board.getPlayer(id.BLACK).getPlayerPieces().get("6,2"), null);
	}
	
	@Test
	/**
	 * Test if user can stay at a position
	 */
	public void noMoveTest() {
          board.makeMove(new Coordinate(1,3), new Coordinate(1,3));
          assertEquals(board.getCell(1, 3).getPieceOnCell().getName(),"Pawn");
          assertEquals(board.getTurn(),id.WHITE);	//Turn did not change. 
	}
	

}
