package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import board.Board;
import board.Coordinate;
import player.Player;
import player.Player.id;


/**
 * Knight -- One of the piece objects that can move as a Knight in chess. 
 * 
 * The knight moves to any of the closest squares that are not on the same rank, file, or diagonal, thus the 
 * move forms an "L"-shape: two squares vertically and one square horizontally, or two squares horizontally and
 * one square vertically. The knight is the only piece that can leap over other pieces.
 *  
 * @author jkim475
 */

public class KnightTest {

	Board board;
	Player playerA;
	Player playerB;
	
	@Before
	public void setup() {
        board = new Board(); 
        playerA = board.getPlayer(id.WHITE); 
        playerA = board.getPlayer(id.BLACK); 
        board.initializeBoard();
	}
	
	@Test
	/**
	 * Tests if the piece can move like a bishop
	 */
	public void validMovementTest() {
        board.makeMove(new Coordinate(0,1), new Coordinate(2,2));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(2,2), new Coordinate(3,4));
        assertEquals(board.getCell(0,1).getPieceOnCell(),null);
        assertEquals(board.getCell(3,4).getPieceOnCell().getName(),"Knight");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("3,4").getName(),"Knight");
	}
	
	@Test
	/**
	 * Tests if the piece can move like other pieces 
	 */
	public void invalidMovementTest() {
        board.makeMove(new Coordinate(0,1), new Coordinate(1,3));
        assertEquals(board.getCell(1,3).getPieceOnCell().getName(),"Pawn");
        assertEquals(board.getCell(0,1).getPieceOnCell().getName(),"Knight");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("1,3").getName(),"Pawn");
	}
	
	@Test
	/**
	 * Tests if piece moves out of bounds. 
	 */
	public void outOfBoundTest() {
		
        board.makeMove(new Coordinate(0,1), new Coordinate(-1,3));
        assertEquals(board.getCell(-1,3).getPieceOnCell(),null);
        assertEquals(board.getCell(0,1).getPieceOnCell().getName(),"Knight");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("0,1").getName(),"Knight");

	}
	
	@Test
	/**
	 * Tests if board cell is set to null and other player loses a piece
	 */
	public void captureTest() {
		assertEquals(board.getPlayer(id.BLACK).getPlayerPieces().get("6,6").getName(), "Pawn");
        board.makeMove(new Coordinate(1,3), new Coordinate(2,3));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(0,2), new Coordinate(5,7));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        board.makeMove(new Coordinate(5,7), new Coordinate(6,6));
        assertEquals(board.getCell(6, 6).getPieceOnCell().getName(),"Bishop");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("6,6").getName(), "Bishop");
        assertEquals(board.getPlayer(id.BLACK).getPlayerPieces().get("6,6"), null);
	}
	
	@Test
	/**
	 * Tests if bishop can leap over enemies or allies.
	 */
	public void leapOverTest() {
        
        board.makeMove(new Coordinate(0,2), new Coordinate(2,0));
        assertEquals(board.getCell(0, 2).getPieceOnCell().getName(),"Bishop");
        assertEquals(board.getCell(2, 0).getPieceOnCell(),null);
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("2,0"),null);
		
	}
	
	@Test
	/**
	 * Test if user can stay at a position
	 */
	public void noMoveTest() {
          board.makeMove(new Coordinate(0,2), new Coordinate(0,2));
          assertEquals(board.getCell(0, 2).getPieceOnCell().getName(),"Bishop");
          assertEquals(board.getTurn(),id.WHITE);	//Turn did not change. 
	}
	
}
