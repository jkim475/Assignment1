package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import board.Board;
import board.Coordinate;
import player.Player;
import player.Player.id;


/**
 * King -- One of the piece objects that can move as a King in chess. 
 * 
 * The king moves one square in any direction.
 *  
 * @author jkim475
 */

public class KingTest {

	Board board;
	Player playerA;
	Player playerB;
	
	@Before
	public void setup() {
        board = new Board(); 
        playerA = board.getPlayer(id.WHITE); 
        playerA = board.getPlayer(id.BLACK); 
        board.initializeBoard();
	}
	
	@Test
	/**
	 * Tests if the piece can move like a bishop
	 */
	public void validMovementTest() {
        board.makeMove(new Coordinate(1,4), new Coordinate(3,4));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(0,4), new Coordinate(1,4));
        assertEquals(board.getCell(0,4).getPieceOnCell(),null);
        assertEquals(board.getCell(1,4).getPieceOnCell().getName(),"King");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("1,4").getName(),"King");
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        board.makeMove(new Coordinate(1,4), new Coordinate(2,3));
        assertEquals(board.getCell(1,4).getPieceOnCell(),null);
        assertEquals(board.getCell(2,3).getPieceOnCell().getName(),"King");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("2,3").getName(),"King");
	}
	
	@Test
	/**
	 * Tests if the piece can move like other pieces 
	 */
	public void invalidMovementTest() {
        board.makeMove(new Coordinate(0,4), new Coordinate(1,4));
        assertEquals(board.getCell(1,3).getPieceOnCell().getName(),"Pawn");
        assertEquals(board.getCell(0,4).getPieceOnCell().getName(),"King");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("1,3").getName(),"Pawn");
	}
	
	@Test
	/**
	 * Tests if piece moves out of bounds. 
	 */
	public void outOfBoundTest() {
		
        board.makeMove(new Coordinate(0,4), new Coordinate(-1,4));
        assertEquals(board.getCell(-1,4).getPieceOnCell(),null);
        assertEquals(board.getCell(0,4).getPieceOnCell().getName(),"King");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("0,4").getName(),"King");

	}
	
	
	@Test
	/**
	 * Test if user can stay at a position
	 */
	public void noMoveTest() {
          board.makeMove(new Coordinate(0,4), new Coordinate(0,4));
          assertEquals(board.getCell(0,4).getPieceOnCell().getName(),"King");
	}
	
}
