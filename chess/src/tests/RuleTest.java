package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import board.Board;
import board.Board.finishCondition;
import board.Cell;
import board.Coordinate;
import player.Player;
import player.Player.id;

public class RuleTest {

	Board board;
	Player playerA;
	Player playerB;
	
	@Before
	public void setup() {
        board = new Board(); 
        playerA = board.getPlayer(id.WHITE); 
        playerA = board.getPlayer(id.BLACK); 
        board.initializeBoard();
	}
	
	   public final static void printBoard(Board board) 
	   {
	        for (Cell[] row : board.getBoard()) {
	        	System.out.println();
	        		for (Cell cell : row)
	        			System.out.print(cell.toString());
	        }
	   }
	
	@Test
	/**
	 * Test if check is returned when king is in danger
	 */
	public void isCheckTest() {
        Board board = new Board(); 
        board.initializeBoard();
        printBoard(board);
        System.out.println();
        assertEquals(board.makeMove(new Coordinate(7,3), new Coordinate(7,7)),finishCondition.CHECK);
        printBoard(board);
        System.out.println();
	}
	
	@Test
	/**
	 * Tests if turn changes after check is resolved
	 */
	public void changeTurnAfterCheck() {

	}
	
	@Test
	/**
	 * test if turn stays the same until check is resolved. 
	 */
	public void doNotChangeTurnAfterCheck() {

	}
	
	@Test
	/**
	 * test check mate situation. 
	 */
	public void checkMateTest() {

	}
	
	@Test
	/**
	 *  test stalemate situation. 
	 */
	public void staleMateTest() {

	}
	
	

}
