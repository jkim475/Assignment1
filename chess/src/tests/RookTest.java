package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import board.Board;
import board.Coordinate;
import player.Player;
import player.Player.id;

/**
 * Rook -- One of the piece objects that can move as a Rook in chess. 
 * 
 * The rook can move any number of squares along any rank or file, but may not leap over other pieces.
 *  
 * @author jkim475
 */


public class RookTest {

	Board board;
	Player playerA;
	Player playerB;
	
	@Before
	public void setup() {
        board = new Board(); 
        playerA = board.getPlayer(id.WHITE); 
        playerA = board.getPlayer(id.BLACK); 
        board.initializeBoard();
	}
	
	@Test
	/**
	 * Tests if the piece can move like a rook.
	 */
	public void validMovementTest() {
        board.makeMove(new Coordinate(1,0), new Coordinate(3,0));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(1,1), new Coordinate(3,1));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        
        board.makeMove(new Coordinate(0,0), new Coordinate(2,0));
        assertEquals(board.getCell(0, 0).getPieceOnCell(),null);
        assertEquals(board.getCell(2, 0).getPieceOnCell().getName(),"Rook");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("2,0").getName(),"Rook");
        
        board.makeMove(new Coordinate(6,2), new Coordinate(5,2));
        
        board.makeMove(new Coordinate(2,0), new Coordinate(2,5));
        assertEquals(board.getCell(2, 0).getPieceOnCell(),null);
        assertEquals(board.getCell(2, 5).getPieceOnCell().getName(),"Rook");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("2,5").getName(),"Rook");
	}
	
	@Test
	/**
	 * Tests if the piece can move like other pieces 
	 */
	public void invalidMovementTest() {
        board.makeMove(new Coordinate(1,0), new Coordinate(3,0));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(1,1), new Coordinate(3,1));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        
        board.makeMove(new Coordinate(0,0), new Coordinate(3,0));
        assertEquals(board.getCell(3, 0).getPieceOnCell().getName(),"Chameleon");
        assertEquals(board.getCell(0, 0).getPieceOnCell().getName(),"Rook");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("0,0").getName(),"Rook");
        
        board.makeMove(new Coordinate(6,2), new Coordinate(5,2));
        
        board.makeMove(new Coordinate(0,0), new Coordinate(1,1));
        assertEquals(board.getCell(0, 0).getPieceOnCell().getName(),"Rook");
        assertEquals(board.getCell(1, 1).getPieceOnCell(),null);
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("0,0").getName(),"Rook");
	}
	
	@Test
	/**
	 * Tests if piece moves out of bounds. 
	 */
	public void outOfBoundTest() {
		
        board.makeMove(new Coordinate(0,0), new Coordinate(-1,0));
        assertEquals(board.getCell(-1, 0).getPieceOnCell(),null);
        assertEquals(board.getCell(0, 0).getPieceOnCell().getName(),"Rook");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("0,0").getName(),"Rook");
	}
	
	@Test
	/**
	 * Tests if board cell is set to null and other player loses a piece
	 */
	public void captureTest() {
        board.makeMove(new Coordinate(1,0), new Coordinate(3,0));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(1,1), new Coordinate(3,1));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        board.makeMove(new Coordinate(0,0), new Coordinate(2,0));
        board.makeMove(new Coordinate(6,2), new Coordinate(5,2));
        board.makeMove(new Coordinate(2,0), new Coordinate(2,5));
        board.makeMove(new Coordinate(5,2), new Coordinate(4,2));
        board.makeMove(new Coordinate(2,5), new Coordinate(6,5));
        assertEquals(board.getCell(2, 5).getPieceOnCell(),null);
        assertEquals(board.getCell(6, 5).getPieceOnCell().getName(),"Rook");
        assertEquals(board.getPlayer(id.BLACK).getPlayerPieces().get("6,5"),null);
	}
	
	@Test
	/**
	 * Tests if Rook can leap over enemies or allies.
	 */
	public void leapOverTest() {
        
        board.makeMove(new Coordinate(1,0), new Coordinate(3,0));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
 
        board.makeMove(new Coordinate(0,0), new Coordinate(5,0));
        assertEquals(board.getCell(5, 0).getPieceOnCell(),null);
        assertEquals(board.getCell(0, 0).getPieceOnCell().getName(),"Rook");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("0,0").getName(),"Rook");
        
		
	}
	
	@Test
	/**
	 * Test if user can stay at a position
	 */
	public void noMoveTest() {
          board.makeMove(new Coordinate(0,0), new Coordinate(0,0));
          assertEquals(board.getCell(0, 0).getPieceOnCell().getName(),"Rook");
          assertEquals(board.getTurn(),id.WHITE);	//Turn did not change. 
	}

}
