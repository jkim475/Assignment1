package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import board.Board;
import board.Coordinate;
import player.Player;
import player.Player.id;

/**
 * Queen -- One of the piece objects that can move as a Queen in chess. 
 * 
 * The queen combines the power of the rook and bishop and can move any number of squares along rank, file, or 
 * diagonal, but it may not leap over other pieces.
 *  
 * @author jkim475
 */

public class QueenTest {
	Board board;
	Player playerA;
	Player playerB;
	
	@Before
	public void setup() {
        board = new Board(); 
        playerA = board.getPlayer(id.WHITE); 
        playerA = board.getPlayer(id.BLACK); 
        board.initializeBoard();
	}
	
	@Test
	/**
	 * Tests if the piece can move like a rook.
	 */
	public void validMovementTest() {
        board.makeMove(new Coordinate(1,3), new Coordinate(2,3));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(2,3), new Coordinate(3,3));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        board.makeMove(new Coordinate(0,3), new Coordinate(2,3));
        board.makeMove(new Coordinate(4,1), new Coordinate(3,1));
        board.makeMove(new Coordinate(2,3), new Coordinate(2,6));
        board.makeMove(new Coordinate(3,1), new Coordinate(2,1));
        board.makeMove(new Coordinate(2,6), new Coordinate(4,4));

        assertEquals(board.getCell(0, 3).getPieceOnCell(),null);
        assertEquals(board.getCell(4, 4).getPieceOnCell().getName(),"Queen");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("4,4").getName(),"Queen");
        
	}
	
	@Test
	/**
	 * Tests if the piece can move like other pieces 
	 */
	public void invalidMovementTest() {
        board.makeMove(new Coordinate(1,3), new Coordinate(2,3));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(2,3), new Coordinate(3,3));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        board.makeMove(new Coordinate(0,3), new Coordinate(2,3));
        board.makeMove(new Coordinate(4,1), new Coordinate(3,1));
        board.makeMove(new Coordinate(2,3), new Coordinate(3,5));
        assertEquals(board.getCell(0, 3).getPieceOnCell(),null);
        assertEquals(board.getCell(2, 3).getPieceOnCell().getName(),"Queen");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("2,3").getName(),"Queen");
	}
	
	@Test
	/**
	 * Tests if piece moves out of bounds. 
	 */
	public void outOfBoundTest() {
		
        board.makeMove(new Coordinate(0,3), new Coordinate(-1,3));
        assertEquals(board.getCell(-1, 3).getPieceOnCell(),null);
        assertEquals(board.getCell(0, 3).getPieceOnCell().getName(),"Queen");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("0,3").getName(),"Queen");
	}
	
	@Test
	/**
	 * Tests if board cell is set to null and other player loses a piece
	 */
	public void captureTest() {
        board.makeMove(new Coordinate(1,3), new Coordinate(2,3));
        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
        board.makeMove(new Coordinate(2,3), new Coordinate(3,3));
        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
        board.makeMove(new Coordinate(0,3), new Coordinate(2,3));
        board.makeMove(new Coordinate(4,1), new Coordinate(3,1));
        board.makeMove(new Coordinate(2,3), new Coordinate(2,6));
        board.makeMove(new Coordinate(3,1), new Coordinate(2,1));
        board.makeMove(new Coordinate(2,6), new Coordinate(6,6));

        assertEquals(board.getCell(0, 3).getPieceOnCell(),null);
        assertEquals(board.getCell(6, 6).getPieceOnCell().getName(),"Queen");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("6,6").getName(),"Queen");
        assertEquals(board.getPlayer(id.BLACK).getPlayerPieces().get("6,6"),null);
	}
	
	@Test
	/**
	 * Tests if Rook can leap over enemies or allies.
	 */
	public void leapOverTest() {
        
        board.makeMove(new Coordinate(0,3), new Coordinate(3,3));

        assertEquals(board.getCell(3, 3).getPieceOnCell(),null);
        assertEquals(board.getCell(0, 3).getPieceOnCell().getName(),"Queen");
        assertEquals(board.getPlayer(id.WHITE).getPlayerPieces().get("0,3").getName(),"Queen");
        
		
	}
	
	@Test
	/**
	 * Test if user can stay at a position
	 */
	public void noMoveTest() {
          board.makeMove(new Coordinate(0,3), new Coordinate(0,3));
          assertEquals(board.getCell(0, 3).getPieceOnCell().getName(),"Queen");
          assertEquals(board.getTurn(),id.WHITE);	//Turn did not change. 
	}


}
