package pieces;

import java.util.ArrayList;
import java.util.List;

import board.Board;
import board.Coordinate;
import player.Player.id;

/**
 * Bishop -- A subclass of Piece which can be instantiated to move as a bishop
 * @author jkim475
 */
public class Bishop extends Piece{
	
	public Bishop(id playerId, Coordinate coord) {
		super(playerId, coord);
		super.setName("Bishop");
	}

    
	/**
	 * @param coord is the coordinate that the piece is on. 
	 * @param board is the board instance that the game is played on.  
	 * @return returns a list of possible <Coordinates> that the rook can go to. 
	 * 
	 * Determines the possible <Coordinates> that the rook can move to. 
	 */
    
    // add coordinates in all possible directions to possibleCoords while not adding coordinates that are out of bounds or leaping over other pieces.
    public List<Coordinate> possibleMoves(Coordinate coord, Board board) {
		List<Coordinate> possibleCoords  = new ArrayList<Coordinate>();
		getNWCoords(coord.x,coord.y,board,possibleCoords);
		getNECoords(coord.x,coord.y,board,possibleCoords);
		getSWCoords(coord.x,coord.y,board,possibleCoords);
		getSECoords(coord.x,coord.y,board,possibleCoords);
		return possibleCoords;
    }

}
