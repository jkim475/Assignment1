package pieces;

import java.util.ArrayList;
import java.util.List;

import board.Board;
import board.Coordinate;
import player.Player.id;
/**
 * Pawn -- A subclass of Piece which can be instantiated to move as a pawn
 * @author jkim475
 */
public class Pawn extends Piece {
	
	boolean firstMove = true; 
	public Pawn(id player,Coordinate coord) {
		super(player,coord);
		super.setName("Pawn");
	}

    
	/**
	 * @param cood is the coordinate that the piece is on. 
	 * @param board is the board instance that the game is played on.  
	 * @return returns a list of possible <Coordinates> that the rook can go to. 
	 * 
	 * Determines the possible <Coordinates> that the pawn can move to. 
	 */
    public List<Coordinate> possibleMoves(Coordinate coord, Board board) {
    		List<Coordinate> possibleCoords  = new ArrayList<Coordinate>();
    		id playerOnCell;
    		// for player A, try out different coordinates and check if they ally piece. If first move is true, also add 2 steps. 
    		if(this.getPlayer() == id.WHITE) {
    			playerOnCell = board.getPlayerId(coord.x+1,coord.y);	
    			if(playerOnCell == null && coord.x < 7) {
    				possibleCoords.add(new Coordinate(coord.x+1,coord.y)); // add 1 up if cell is empty
    				if(firstMove == true) {
    					playerOnCell = board.getPlayerId(coord.x+2,coord.y);
    					if(playerOnCell == null)
    						possibleCoords.add(new Coordinate(coord.x+2,coord.y)); // add 2 up if firstmove is true and empty
    				}
    			}
    			playerOnCell = board.getPlayerId(coord.x+1,coord.y+1);
    			if(playerOnCell == getOpponentPlayer()) {
    				possibleCoords.add(new Coordinate(coord.x+1,coord.y+1));
    			}
    			playerOnCell = board.getPlayerId(coord.x+1,coord.y-1);
    			if(playerOnCell == getOpponentPlayer()) {
    				possibleCoords.add(new Coordinate(coord.x+1,coord.y-1));
    			}
    		}
    		else {
    			// if the player is B, try different coordinates with same logic as player B pawns go from bottom to up. 
    			playerOnCell = board.getPlayerId(coord.x-1,coord.y);
    			if(playerOnCell == null && coord.x > 0){
    				possibleCoords.add(new Coordinate(coord.x-1,coord.y)); 
    				if(firstMove == true) {
    					playerOnCell = board.getPlayerId(coord.x-2,coord.y);
    					if(playerOnCell == null)
    						possibleCoords.add(new Coordinate(coord.x-2,coord.y)); 
    				}
    			}
    			playerOnCell = board.getPlayerId(coord.x-1,coord.y-1);
    			if(playerOnCell != getPlayer() && playerOnCell != null) {
    				possibleCoords.add(new Coordinate(coord.x-1,coord.y-1));
    			}
    			playerOnCell = board.getPlayerId(coord.x-1,coord.y+1);
    			if(playerOnCell != getPlayer() && playerOnCell != null) {
    				possibleCoords.add(new Coordinate(coord.x-1,coord.y+1));
    			}
    		}
    		
    		return possibleCoords;
    }
	

}
