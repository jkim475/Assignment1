package pieces;

import java.util.ArrayList;
import java.util.List;

import board.Board;
import board.Coordinate;
import player.Player.id;

/**
 * Dabbaba -- A subclass of Piece which can be instantiated to move as a Dabbaba
 * @author jkim475
 */

public class Dabbaba extends Piece{
	public Dabbaba(id playerId, Coordinate coord) {
		super(playerId, coord);
		super.setName("Dabbaba");
	}
	
    
	/**
	 * @param coord is the coordinate that the piece is on. 
	 * @param board is the board instance that the game is played on.  
	 * @return returns a list of possible <Coordinates> that the Dabbaba can go to. 
	 * 
	 * Determines the possible <Coordinates> that the Dabbaba can move to. 
	 */
    
    // add coordinates in all possible directions to possibleCoords while not adding coordinates that are out of bounds or leaping over other pieces.
    public List<Coordinate> possibleMoves(Coordinate coord, Board board) {
		List<Coordinate> coordsToTry  = new ArrayList<Coordinate>();
		List<Coordinate> possibleCoords  = new ArrayList<Coordinate>();
				
		// add coordinates in all possible directions
		if(coord.x<6) coordsToTry.add(new Coordinate(coord.x+2,coord.y));
		if(coord.x>1) coordsToTry.add(new Coordinate(coord.x+2,coord.y));
		if(coord.y<6) coordsToTry.add(new Coordinate(coord.x,coord.y+2));
		if(coord.y>1) coordsToTry.add(new Coordinate(coord.x,coord.y-2));
		
		id playerOnCell;
		
		// push possible coordinates to returning list if there is no same team on destination
		for(int i = 0; i<coordsToTry.size(); i++) {
			int xCoord = coordsToTry.get(i).x;
			int yCoord = coordsToTry.get(i).y;
			playerOnCell = board.getPlayerId(xCoord, yCoord);
			if(getPlayer() != playerOnCell)
				possibleCoords.add(new Coordinate(xCoord,yCoord)); 
		}
	
		return possibleCoords;
    }
}
