package pieces;

import java.util.ArrayList;
import java.util.List;

import board.Board;
import board.Coordinate;
import player.Player.id;
/**
 * Rook -- A subclass of Piece which can be instantiated to move as a rook
 * @author jkim475
 */
public class Rook extends Piece{
	
	public Rook(id playerId, Coordinate coord) {
		super(playerId, coord);
		super.setName("Rook");
	}
	
	/**
	 * @param coord is the coordinate that the piece is on. 
	 * @param board is the board instance that the game is played on.  
	 * @return returns a list of possible <Coordinates> that the rook can go to. 
	 * 
	 * Determines the possible <Coordinates> that the rook can move to. 
	 */
    
    // add coordinates in all possible directions to possibleCoords while not adding coordinates that are out of bounds or leaping over other pieces.
    public List<Coordinate> possibleMoves(Coordinate coord, Board board) {
		List<Coordinate> possibleCoords  = new ArrayList<Coordinate>();
		getNCoords(coord.x,coord.y,board,possibleCoords);
		getSCoords(coord.x,coord.y,board,possibleCoords);
		getWCoords(coord.x,coord.y,board,possibleCoords);
		getECoords(coord.x,coord.y,board,possibleCoords);
		return possibleCoords;
    }
	
}
