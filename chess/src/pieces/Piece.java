package pieces;

import java.util.HashMap;
import java.util.List;

import board.Board;
import board.Coordinate;
import player.Player.id;
/**
 * Piece -- super class of all chess pieces that provides variables and functions commonly used by all pieces. 
 * @author jkim475
 */

// see each of the specific implementations of each piece to see specific logic.
public class Piece {
	
	// name of the piece
	private String Name = "None";
	//id of the player a piece belongs to
	protected id Player = null;
	// the coordinates of this piece
	private Coordinate coord = null; 
	
	public Piece(id playerId, Coordinate coord) {
		this.coord = coord; 
		if(playerId == id.WHITE)
			this.Player = id.WHITE;
		else
			this.Player = id.BLACK; 
	}

	public String getName() {
    		return this.Name;
    }
    
	public void setName(String name) {
		this.Name =  name;
	}
	
	public id getPlayer() {
		return this.Player;
	}
	
	public id getOpponentPlayer() {
		if(this.Player == id.WHITE)
			return id.BLACK;
		else if (this.Player == id.BLACK)
			return id.WHITE; 
		else
			return null;
	}
	
	public Coordinate getCoord() {
		return this.coord; 
	}
	public void setCoord(Coordinate coord) {
		this.coord = coord; 
	}
	
	/**
	 * @param moveToCoord is the coordinate the piece is requested to move to. 
	 * @param board is the board instance that the game is played on. 
	 * @param playerId is a enum or a tag that tells who's turn it is. 
	 * @return returns the boolean value of whether move was successful. 
	 * 
	 * Requests the rook to be moved on a board to for a certian player to a destination coordinate. 
	 */
    public boolean move(Coordinate moveToCoord, Board board, id player) {
		boolean moved = false;
	    List<Coordinate> possibleCoords = possibleMoves(getCoord(),board); // determine possible coordinates that the pawn can move to. 
		for (Coordinate  coord: possibleCoords) {
			if(moveToCoord.equals(coord)) {
				// enter only if the destination coordinate exists, and pawn cannot move 2steps after first move. 
				moved = true;
				// update board and player's pieces according to successful move. 
				board.setCell(getCoord(), null);
				updatePieceCoord(moveToCoord,board);
				captureIfOpponentExists(moveToCoord,board);
				board.setCell(coord, this);
				return moved;
			}
		}
		return moved;
    }
    
    public List<Coordinate> possibleMoves(Coordinate coord, Board board) {
		return null;
    }
    
    void captureIfOpponentExists(Coordinate moveToCoord, Board board) {
    	
    		HashMap<String, Piece> opponentPlayerPieces = board.getPlayer(getOpponentPlayer()).getPlayerPieces();
    	
    		if(opponentPlayerPieces.containsKey(moveToCoord.code)){
    			if(this instanceof Chameleon) {
    				this.Name = opponentPlayerPieces.get(moveToCoord.code).getName();
    				System.out.println(Name);
    			}
    			opponentPlayerPieces.remove(moveToCoord.code);
    		}
    	
    }
    
	/**
	 * @param moveToCoord is the coordinate the piece is requested to move to. 
	 * @param board is the board instance that the game is played on. 
	 * 
	 * updates information in a player's piece according to movement
	 */
    
    void updatePieceCoord(Coordinate moveToCoord, Board board){
    	 HashMap<String, Piece> playerPieces = board.getPlayer(this.Player).getPlayerPieces();
    	 playerPieces.put(moveToCoord.code,playerPieces.get(this.coord.code));
    	 playerPieces.remove(this.coord.code);
    	 this.coord = moveToCoord;  	 
    } 
    
	/**
	 * @param xCoord is the x coordinate of the piece
	 * @param yCoord is the y coordinate of the piece
	 * @param board is the current board chess  is played on
	 * @param possibleCoords is the coordinates that should be updated that a piece may move to. 
	 * 
	 * adds possible movement coordinates for a piece
	 */
    
    void getSCoords(int xCoord,int yCoord, Board board, List<Coordinate> possibleCoords) {
		while(yCoord < 7) { //out of bounds condition
			yCoord += 1;
			id playerOnCell = board.getPlayerId(xCoord,yCoord);
			if(getPlayer() != playerOnCell) { // if a piece from same team is on cell, stop adding. 
				possibleCoords.add(new Coordinate(xCoord,yCoord));
			}
			if(playerOnCell != null)
				break;
		}
    } 
    
	/**
	 * @param xCoord is the x coordinate of the piece
	 * @param yCoord is the y coordinate of the piece
	 * @param board is the current board chess  is played on
	 * @param possibleCoords is the coordinates that should be updated that a piece may move to. 
	 * 
	 * adds possible movement coordinates for a piece
	 */
    
    void getNCoords(int xCoord,int yCoord, Board board, List<Coordinate> possibleCoords) {
		while(yCoord > 0) { //out of bounds condition
			yCoord -= 1;
			id playerOnCell = board.getPlayerId(xCoord,yCoord);
			if(getPlayer() != playerOnCell) { // if a piece from same team is on cell, stop adding. 
				possibleCoords.add(new Coordinate(xCoord,yCoord));
			}
			if(playerOnCell != null)
				break;
		}
    }
    
	/**
	 * @param xCoord is the x coordinate of the piece
	 * @param yCoord is the y coordinate of the piece
	 * @param board is the current board chess  is played on
	 * @param possibleCoords is the coordinates that should be updated that a piece may move to. 
	 * 
	 * adds possible movement coordinates for a piece
	 */
    void getECoords(int xCoord,int yCoord, Board board, List<Coordinate> possibleCoords) { 
		while(xCoord < 7) { //out of bounds condition
			xCoord += 1;
			id playerOnCell = board.getPlayerId(xCoord,yCoord);
			if(getPlayer() != playerOnCell) { // if a piece from same team is on cell, stop adding. 
				possibleCoords.add(new Coordinate(xCoord,yCoord));
			}
			if(playerOnCell != null)
				break;
		}
    } 
    
	/**
	 * @param xCoord is the x coordinate of the piece
	 * @param yCoord is the y coordinate of the piece
	 * @param board is the current board chess  is played on
	 * @param possibleCoords is the coordinates that should be updated that a piece may move to. 
	 * 
	 * adds possible movement coordinates for a piece
	 */
    void getWCoords(int xCoord,int yCoord, Board board, List<Coordinate> possibleCoords) { 
    		id playerOnCell;
    		while(xCoord > 0) { //out of bounds condition
			xCoord -= 1;
			playerOnCell = board.getPlayerId(xCoord,yCoord);
			if(getPlayer() != playerOnCell) { // if a piece from same team is on cell, stop adding. 
				possibleCoords.add(new Coordinate(xCoord,yCoord));
			}
			if(playerOnCell != null)
				break;
		}
    } 
    
	/**
	 * @param xCoord is the x coordinate of the piece
	 * @param yCoord is the y coordinate of the piece
	 * @param board is the current board chess  is played on
	 * @param possibleCoords is the coordinates that should be updated that a piece may move to. 
	 * 
	 * adds possible movement coordinates for a piece
	 */
    void getNECoords(int xCoord,int yCoord, Board board, List<Coordinate> possibleCoords) {
		id playerOnCell;
		while(xCoord < 7 && yCoord > 0) { //out of bounds condition
			xCoord += 1;
			yCoord -= 1;
			playerOnCell = board.getPlayerId(xCoord,yCoord);
			if(getPlayer() != playerOnCell) {
				possibleCoords.add(new Coordinate(xCoord,yCoord));
			}
			if(playerOnCell != null)  // if a piece from same team is on cell, stop adding. 
				break;
		}
    	} 
	/**
	 * @param xCoord is the x coordinate of the piece
	 * @param yCoord is the y coordinate of the piece
	 * @param board is the current board chess  is played on
	 * @param possibleCoords is the coordinates that should be updated that a piece may move to. 
	 * 
	 * adds possible movement coordinates for a piece
	 */
    void getNWCoords(int xCoord,int yCoord, Board board, List<Coordinate> possibleCoords) { 
		id playerOnCell;
		while(xCoord > 0 && yCoord > 0) { //out of bounds condition
			xCoord -= 1;
			yCoord -= 1;
			playerOnCell = board.getPlayerId(xCoord,yCoord);
			if(getPlayer() != playerOnCell) {
				possibleCoords.add(new Coordinate(xCoord,yCoord));
			}
			if(playerOnCell != null)  // if a piece from same team is on cell, stop adding. 
				break;
		}
    }
    
	/**
	 * @param xCoord is the x coordinate of the piece
	 * @param yCoord is the y coordinate of the piece
	 * @param board is the current board chess  is played on
	 * @param possibleCoords is the coordinates that should be updated that a piece may move to. 
	 * 
	 * adds possible movement coordinates for a piece
	 */
    void getSECoords(int xCoord,int yCoord, Board board, List<Coordinate> possibleCoords) { 
    		id playerOnCell;
    		while(xCoord < 7 && yCoord < 7) { //out of bounds condition
			xCoord += 1;
			yCoord += 1;
			playerOnCell = board.getPlayerId(xCoord,yCoord);
			if(getPlayer() != playerOnCell) {
				possibleCoords.add(new Coordinate(xCoord,yCoord));
			}
			if(playerOnCell != null)  // if a piece from same team is on cell, stop adding. 
				break;
		}
    } 
    
	/**
	 * @param xCoord is the x coordinate of the piece
	 * @param yCoord is the y coordinate of the piece
	 * @param board is the current board chess  is played on
	 * @param possibleCoords is the coordinates that should be updated that a piece may move to. 
	 * 
	 * adds possible movement coordinates for a piece
	 */
    void getSWCoords(int xCoord,int yCoord, Board board, List<Coordinate> possibleCoords) { 
		id playerOnCell;
		while(xCoord > 0 && yCoord < 7) { //out of bounds condition
			xCoord -= 1;
			yCoord += 1;
			playerOnCell = board.getPlayerId(xCoord,yCoord);
			if(getPlayer() != playerOnCell) {
				possibleCoords.add(new Coordinate(xCoord,yCoord));
			}
			if(playerOnCell != null)  // if a piece from same team is on cell, stop adding. 
				break;
		}
    } 
}
