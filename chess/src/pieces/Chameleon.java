package pieces;

import java.util.ArrayList;
import java.util.List;

/**
 * Chameleon -- A subclass of Piece which can be instantiated to move as a Chameleon
 * @author jkim475
 */

import board.Board;
import board.Coordinate;
import player.Player.id;

public class Chameleon extends Piece{
	
	public Chameleon(id playerId, Coordinate coord) {
		super(playerId, coord);
		super.setName("Chameleon");
	}

	Piece actualPiece; 
    
	/**
	 * @param coord is the coordinate that the piece is on. 
	 * @param board is the board instance that the game is played on.  
	 * @return returns a list of possible <Coordinates> that the chameleon can go to. 
	 * 
	 * Determines the possible <Coordinates> that the chameleon can move to. 
	 */
    
    // add possible moves to be that of the captured piece
    public List<Coordinate> possibleMoves(Coordinate coord, Board board) {
    		switch (this.getName()) {
		    case "Pawn": actualPiece = new Pawn(Player,coord);
			case "Bishop": actualPiece = new Bishop(Player,coord);
			case "Rook":  actualPiece = new Rook(Player,coord);
			case "Knight":  actualPiece = new Knight(Player,coord);
			case "Queen": actualPiece = new Queen(Player,coord);
			default: actualPiece = new Pawn(Player,coord); 			
			return actualPiece.possibleMoves(coord, board);
    		}
    }

}

