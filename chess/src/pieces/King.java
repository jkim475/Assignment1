package pieces;

/**
 * King -- A subclass of Piece which can be instantiated to move as a king
 * @author jkim475
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import board.Board;
import board.Coordinate;
import player.Player.id;

public class King extends Piece{
	
	public King(id playerId, Coordinate coord) {
		super(playerId, coord);
		super.setName("King");
	}
	
    
	/**
	 * @param coord is the coordinate that the piece is on. 
	 * @param board is the board instance that the game is played on.  
	 * @return returns a list of possible <Coordinates> that the rook can go to. 
	 * 
	 * Determines the possible <Coordinates> that the rook can move to. 
	 */
    public List<Coordinate> possibleMoves(Coordinate coord, Board board) {
		List<Coordinate> coordsToTry  = new ArrayList<Coordinate>();
		List<Coordinate> possibleCoords  = new ArrayList<Coordinate>();
		List<Coordinate> impossibleCoords  = new ArrayList<Coordinate>();
		if(coord.x<7)
		coordsToTry.add(new Coordinate(coord.x+1,coord.y));
		if(coord.x>0)
		coordsToTry.add(new Coordinate(coord.x-1,coord.y));
		if(coord.y<7)
		coordsToTry.add(new Coordinate(coord.x,coord.y+1));
		if(coord.y>0)
		coordsToTry.add(new Coordinate(coord.x,coord.y-1));
		if(coord.x<7 && coord.y<7)
		coordsToTry.add(new Coordinate(coord.x+1,coord.y+1));
		if(coord.x>0 && coord.y<7)
		coordsToTry.add(new Coordinate(coord.x-1,coord.y+1));
		if(coord.y<7 && coord.x>0)
		coordsToTry.add(new Coordinate(coord.x+1,coord.y-1));
		if(coord.y>0 && coord.x>0)
		coordsToTry.add(new Coordinate(coord.x-1,coord.y-1));
		
		
		id playerOnCell;
		
		for(int i = 0; i<coordsToTry.size(); i++) {
			int xCoord = coordsToTry.get(i).x;
			int yCoord = coordsToTry.get(i).y;
			playerOnCell = board.getPlayerId(xCoord, yCoord);
			if(getPlayer() != playerOnCell)
				possibleCoords.add(new Coordinate(xCoord,yCoord)); 
		}
		possibleCoords.add(new Coordinate(coord.x,coord.y));
		
		playerOnCell = board.getPlayerId(coord.x,coord.y);
		HashMap<String, Piece> pieces;
		// get opponent pieces
		if(playerOnCell == id.WHITE) pieces = board.getPlayer(id.BLACK).getPlayerPieces();
		else pieces = board.getPlayer(id.WHITE).getPlayerPieces();

		Iterator it = pieces.entrySet().iterator();
	    while (it.hasNext()) { 
	    	Map.Entry pair = (Map.Entry)it.next();
	    	String keyCoord =  (String) pair.getKey();
	    	if(((Piece) pair.getValue()).getName() != "King")
	    		// loops through pieces on the board and adds all coords in danger including enemy King's position.
	        impossibleCoords.addAll(pieces.get(keyCoord).possibleMoves(pieces.get(keyCoord).getCoord(), board));
	    		for(Coordinate coords: impossibleCoords) {
	    			//System.out.println("impossibleCoordinates: "+coords.x+"."+coords.y);
	    		}
	    }
		
	    // remove from the set of impossibleCoordinates of the king from the set of possible coordinates. 
		Iterator<Coordinate> i = possibleCoords.iterator();
		while (i.hasNext()) {
			if(impossibleCoords.contains(i.next())) i.remove();
		}
		return possibleCoords;
    }

}
