package pieces;

import java.util.ArrayList;
import java.util.List;

import board.Board;
import board.Coordinate;
import player.Player.id;

/**
 * Knight -- A subclass of Piece which can be instantiated to move as a knight
 * @author jkim475
 */
public class Knight extends Piece{
	
	public Knight(id playerId, Coordinate coord) {
		super(playerId,coord);
		super.setName("Knight");
	}
    
	/**
	 * @param cood is the coordinate that the piece is on. 
	 * @param board is the board instance that the game is played on.  
	 * @return returns a list of possible <Coordinates> that the Knight can go to. 
	 * 
	 * Determines the possible <Coordinates> that the rook can move to. 
	 */
    public List<Coordinate> possibleMoves(Coordinate coord, Board board) {
    		List<Coordinate> coordsToTry  = new ArrayList<Coordinate>();
		List<Coordinate> possibleCoords  = new ArrayList<Coordinate>();
				
		// add coordinates in all possible directions
		if(coord.x<6 && coord.y<7) coordsToTry.add(new Coordinate(coord.x+2,coord.y+1));
		if(coord.x<6 && coord.y>0) coordsToTry.add(new Coordinate(coord.x+2,coord.y-1));
		if(coord.x>1 && coord.y<7) coordsToTry.add(new Coordinate(coord.x-2,coord.y+1));
		if(coord.x>1 && coord.y>0) coordsToTry.add(new Coordinate(coord.x-2,coord.y-1));
		if(coord.x<7 && coord.y<6) coordsToTry.add(new Coordinate(coord.x+1,coord.y+2));
		if(coord.x<7 && coord.y>1) coordsToTry.add(new Coordinate(coord.x+1,coord.y-2));
		if(coord.x>0 && coord.y<6) coordsToTry.add(new Coordinate(coord.x-1,coord.y+2));
		if(coord.x>0 && coord.y>1) coordsToTry.add(new Coordinate(coord.x-1,coord.y-2));
		
		id playerOnCell;
		
		// push possible coordinates to returning list if there is no same team on destination
		for(int i = 0; i<coordsToTry.size(); i++) {
			int xCoord = coordsToTry.get(i).x;
			int yCoord = coordsToTry.get(i).y;
			playerOnCell = board.getPlayerId(xCoord, yCoord);
			if(getPlayer() != playerOnCell)
				possibleCoords.add(new Coordinate(xCoord,yCoord)); 
		}

		return possibleCoords;

    }

}
