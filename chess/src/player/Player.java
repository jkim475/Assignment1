package player;
/**
 * Player -- A class that represents a player for the chess game. Includes pieces that the player has for a game. 
 * @author jkim475
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import board.Board;
import board.Cell;
import board.Coordinate;
import pieces.Bishop;
import pieces.Chameleon;
import pieces.Dabbaba;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

public class Player{
	
	// player id. A and B represent the two players for chess. 
	public enum id {
	    WHITE,BLACK // TODO make black and white
	} 
	
	public id playerId;
	
	// set to true if the king is in danger. 
	public boolean isCheck = false;

	// the pieces that the player currently has. 
	HashMap<String,Piece> pieces = new HashMap<String, Piece>();

	
	/**
	 * @param player is the id of the player that should be initialized.
	 * @return a player that has all the initial pieces necessary for playing chess game. 
	 * 
	 * Constructor for the player class. 
	 */
	
	public Player(id player) { 
		// add initial pieces to the player's list of pieces. This list will shrink if opponent captures the players piece.
			initializePlayerPieces(player);
		  }

	
	/**
	 * @return returns pieces that the player has. The piece in hashmap is accessed with a string code for a coordinate. E.g.getPlayerPieces().get("2,3")
	 * 
	 * Constructor for the player class. 
	 */
	public HashMap<String,Piece> getPlayerPieces(){
		return pieces;
	}

	/**
	 * @param board is the board that the game is played on
	 * @return returns the possible moves for this player's king. Used for checking game end conditions. 
	 * 
	 * Constructor for the player class. 
	 */
	public List<Coordinate> getKingMoves(Board board) {
		
		Iterator it = pieces.entrySet().iterator();
	    while (it.hasNext()) { 
	    	Map.Entry pair = (Map.Entry)it.next();
	    	Piece piece = (Piece) pair.getValue();
	    	if(piece instanceof King) {
	    		List<Coordinate> possibleMov = piece.possibleMoves(piece.getCoord(), board);
	    		isCheck = !possibleMov.contains(piece.getCoord());
	    		//System.out.print("King Coordinate is: " + isCheck);
	    		return possibleMov;
	    		}
	    }
		return null;
	}
	
	   public final static boolean isCheckMate(Board board) {
		   
		   return false; 
	   } 

	   
		private void initializePlayerPieces(id player) {
			if(player == id.WHITE) {
				playerId = id.WHITE;
//					pieces.put("7,3",new Rook(playerId,new Coordinate(7,3)));	 // for check testing
					pieces.put("0,0",new Rook(playerId,new Coordinate(0,0)));	
			//	pieces.put("0,0",new Dabbaba(playerId,new Coordinate(0,0)));	
				pieces.put("0,1",new Knight(playerId,new Coordinate(0,1)));
			    pieces.put("0,2",new Bishop(playerId,new Coordinate(0,2)));
			    pieces.put("0,3",new Queen(playerId,new Coordinate(0,3)));
			    pieces.put("0,4",new King(playerId,new Coordinate(0,4)));
			    pieces.put("0,5",new Bishop(playerId,new Coordinate(0,5)));
			    pieces.put("0,6",new Knight(playerId,new Coordinate(0,6)));
			    pieces.put("0,7",new Rook(playerId,new Coordinate(0,7)));	
			    pieces.put("1,0",new Pawn(playerId,new Coordinate(1,0)));
			//    pieces.put("1,0",new Chameleon(playerId,new Coordinate(1,0)));
			    pieces.put("1,1",new Pawn(playerId,new Coordinate(1,1)));
			    pieces.put("1,2",new Pawn(playerId,new Coordinate(1,2)));
			    pieces.put("1,3",new Pawn(playerId,new Coordinate(1,3)));
			    pieces.put("1,4",new Pawn(playerId,new Coordinate(1,4)));
			    pieces.put("1,5",new Pawn(playerId,new Coordinate(1,5)));
			    pieces.put("1,6",new Pawn(playerId,new Coordinate(1,6)));
			    pieces.put("1,7",new Pawn(playerId,new Coordinate(1,7)));
				
				
			}
			else {
				playerId = id.BLACK; 
//					pieces.put("3,7",new King(playerId,new Coordinate(3,7)));	 // for check testing
				pieces.put("7,0",new Rook(playerId,new Coordinate(7,0)));	
				pieces.put("7,1",new Knight(playerId,new Coordinate(7,1)));
			    pieces.put("7,2",new Bishop(playerId,new Coordinate(7,2)));
			    pieces.put("7,3",new Queen(playerId,new Coordinate(7,3)));
			    pieces.put("7,4",new King(playerId,new Coordinate(7,4)));
			    pieces.put("7,5",new Bishop(playerId,new Coordinate(7,5)));
			    pieces.put("7,6",new Knight(playerId,new Coordinate(7,6)));
			    pieces.put("7,7",new Rook(playerId,new Coordinate(7,7)));	
			    pieces.put("6,0",new Pawn(playerId,new Coordinate(6,0)));
			    pieces.put("6,1",new Pawn(playerId,new Coordinate(6,1)));
			    pieces.put("6,2",new Pawn(playerId,new Coordinate(6,2)));
			    pieces.put("6,3",new Pawn(playerId,new Coordinate(6,3)));
			    pieces.put("6,4",new Pawn(playerId,new Coordinate(6,4)));
			    pieces.put("6,5",new Pawn(playerId,new Coordinate(6,5)));
			    pieces.put("6,6",new Pawn(playerId,new Coordinate(6,6)));
			    pieces.put("6,7",new Pawn(playerId,new Coordinate(6,7)));
			}
		} 

}
