package board;

import pieces.Piece;
import player.Player.id;
/**
 * Cell -- A class which describes an individual cell on the board which can hold a piece. 
 * @author jkim475
 */
public class Cell {
	boolean occupied = false;
	// piece on cell
	Piece pieceOnCell = null; 
	public Piece getPieceOnCell() {
		return this.pieceOnCell;
	}
	// for printing the board to terminal. "_" if there is not piece, and initials of piece if piece exists.
	public final String toString() {
		if(this.pieceOnCell == null) {  //changed
			return " _  ";
		}
		else {
			return pieceOnCell.getName().substring(0, 2) + pieceOnCell.getPlayer().toString().substring(0, 1) + " ";
		}

	} 
}
