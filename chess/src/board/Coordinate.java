package board;

/**
 * Coordinate -- A class like tuple which holds a pair of of coordinates. 
 * @author jkim475
 */
public class Coordinate {
	  public final int x; 
	  public final int y; 
	  public final String code;
	  public Coordinate(int x, int y) { 
	    this.x = x; 
	    this.y = y; 
	    this.code = x + "," + y;
	  } 
	  
	  @Override
	  public boolean equals(Object obj) {
	      if (obj == null) {
	          return false;
	      }
	      if (!Coordinate.class.isAssignableFrom(obj.getClass())) {
	          return false;
	      }
	      final Coordinate other = (Coordinate) obj;
	      if (this.x != other.x) {
	          return false;
	      }
	      if (this.y != other.y) {
	          return false;
	      }
	      return true;
	  }
	  
}
