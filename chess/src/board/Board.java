package board;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pieces.King;
import pieces.Piece;
import player.Player;
import player.Player.id;

/**
 * Board -- A class which describes the board that the game is played on. 
 * @author jkim475
 */
public class Board {
	// dimensions for the board. Can be changed for bigger board. 
	int dimRow = 8; 
	int dimCol = 8; 
	
	// player id that shows whose turn it is. 
	private id turn = id.WHITE; 
	
	// players for this game. 
	Player playerA; 
	Player playerB;
	
	// check conditions for game end conditions. 
	public enum finishCondition {
	    CHECK,CHECKMATE,STALEMATE,NOTFINISHED
	}
	
	// a board which is a 2-d array of cells. 
	Cell[][] board = new Cell[dimRow][dimCol];
	  public Board() { 
			for(int row=0; row<dimRow; row++)
			    for(int col=0; col<dimCol; col++)
			        this.board[row][col] = new Cell();
		  } 
	
    public Cell[][] getBoard() {
		return this.board;
    }
    
   public final Cell getCell(int x, int y) 
   {
	  try {
		  return this.board[x][y];
	  } catch (IndexOutOfBoundsException e) {
	    return new Cell();
	}
   }
   
   public final id getPlayerId(int x, int y) 
   {
		  try {
			  Piece piece = this.board[x][y].getPieceOnCell();
			  if(piece != null) return piece.getPlayer();
		  } catch (IndexOutOfBoundsException e) {
		    return null;
		  }
		 
		  return null; 
   }

   public final void setCell(Coordinate coord, Piece piece) 
   {
	  if(piece == null) {
		  this.board[coord.x][coord.y].occupied = false;
		  this.board[coord.x][coord.y].pieceOnCell = null; 
	  }
	  else {
	      this.board[coord.x][coord.y].occupied = true; 
	      this.board[coord.x][coord.y].pieceOnCell = piece; 
	  }
   }
   
	/**
	 * @param fromCoord is the coordinate that the piece is on. 
	 * @param toCoord is the board instance that the game is played on.  
	 * @return returns an enum finishCondition which is NOTFINISHED, CHECK, or CHECKMATE 
	 * 
	 * Determines the possible <Coordinates> that the rook can move to. 
	 */
   public final finishCondition makeMove(Coordinate fromCoord,Coordinate toCoord) {
	   Piece curPiece = this.board[fromCoord.x][fromCoord.y].pieceOnCell; 
	   // if there is no piece on the cell or it is not the player's turn, do not move. 
	   if(curPiece == null || curPiece.getPlayer() != turn) return finishCondition.NOTFINISHED; 
	   id opponentPlayer = curPiece.getOpponentPlayer();
	   // actually move the piece. 
	   boolean isMoved = curPiece.move(toCoord, this , curPiece.getPlayer());
	   if(isMoved) {
		   // if move was successful, check if the game is finished. 
		   finishCondition isGameFinished = isGameFinished(opponentPlayer);
		   // the turn is changed to opponent player before returning. 
		   this.turn = opponentPlayer;
		   return isGameFinished;
	   }
	   else return finishCondition.NOTFINISHED; 
   }
   public Player getPlayer(id playerId) {
		// TODO Auto-generated method stub
		if(playerId == id.WHITE) return playerA;
		else return playerB;
	}
	   
   
   public final finishCondition isGameFinished(id opponent) {
	   // get opponent king, get possible moves.
	   //loop through all other pieces of opponent and see if king does not exist. 
	   // if king does not exist but possible move size is not 0, it is check
	   // if size is 0 it is checkmate
	   // if size is 1 and it is king, it is stalemate. 
	   // if there are possible moves, continue 
	   List<Coordinate> kingMoves = this.getPlayer(opponent).getKingMoves(this);
	   if(this.getPlayer(opponent).isCheck == true) return finishCondition.CHECK;
	   //if(kingMoves.size() == 0) return finishCondition.CHECKMATE;
	  // else if(kingMoves.size() == 1 && this.board[kingMoves.get(0).x][kingMoves.get(0).y].pieceOnCell.getName()  != "King") return finishCondition.CHECK;
	   //else if(kingMoves.size() == 1 && kingMoves.get(0)kingMoves.pieceofcell == King) return checkStalemate();
	   else return finishCondition.NOTFINISHED; 
   }
   
	/**
	 * Initializes the current board to have the pieces that the players start with by looping through each player's piece and setting the board cells.  
	 */
   public final void initializeBoard() 
   {
	   playerA = new Player(id.WHITE); 
	   playerB = new Player(id.BLACK);
	   
	   HashMap<String, Piece> piecesOfA = playerA.getPlayerPieces();
	   HashMap<String, Piece> piecesOfB = playerB.getPlayerPieces();
	   
	   // iterate through playerA's list of pieces and set board cells with pieces.
		Iterator itA = piecesOfA.entrySet().iterator();
	    while (itA.hasNext()) { 
	    	Map.Entry pair = (Map.Entry)itA.next();
	    	Coordinate pieceCoord = ((Piece) pair.getValue()).getCoord();
	    	Piece piece = (Piece) pair.getValue();
	    	this.setCell(pieceCoord, piece); 
	    }
	
	    // iterate through playerB's list of pieces and set board cells with pieces.
		Iterator itB = piecesOfB.entrySet().iterator();
	    while (itB.hasNext()) { 
	    	Map.Entry pair = (Map.Entry)itB.next();
	    	Coordinate pieceCoord = ((Piece) pair.getValue()).getCoord();
	    	Piece piece = (Piece) pair.getValue();
	    	this.setCell(pieceCoord, piece); 
	    }
   }

   public final id getTurn() {
	   return turn;   
   }
   

}
