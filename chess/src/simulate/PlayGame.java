package simulate;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import board.Board;
import board.Cell;
import board.Coordinate;
import board.Board.finishCondition;
import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;
import player.Player;
import player.Player.id;

public class PlayGame {
	

	   
	   public final static void printBoard(Board board) 
	   {
	        for (Cell[] row : board.getBoard()) {
	        	System.out.println();
	        		for (Cell cell : row)
	        			System.out.print(cell.toString());
	        }
	   }
	   

	   
	  
	   public final static void testRook() 
	   {
	        Board board = new Board(); 
	        board.initializeBoard();
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(1,0), new Coordinate(2,0));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(2,0), new Coordinate(3,0));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(5,1), new Coordinate(4,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(1,1), new Coordinate(2,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(4,1), new Coordinate(3,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(2,1), new Coordinate(3,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(3,1), new Coordinate(2,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(3,1), new Coordinate(4,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(0,0), new Coordinate(2,0));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(2,0), new Coordinate(2,4));
	        printBoard(board);
	        System.out.println();
//	        board.makeMove(new Coordinate(2,4), new Coordinate(6,4));
//	        printBoard(board);
//	        System.out.println();
	        board.makeMove(new Coordinate(6,4), new Coordinate(7,4));
	        printBoard(board);
	        System.out.println();
	   }
	   
	   public final static void testBishop() 
	   {
	        Board board = new Board(); 
	        board.initializeBoard();
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(1,3), new Coordinate(2,3));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(2,3), new Coordinate(3,3));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(0,2), new Coordinate(5,7));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(5,7), new Coordinate(6,6));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(6,6), new Coordinate(7,5));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(7,5), new Coordinate(6,4));
	        printBoard(board);
	        board.makeMove(new Coordinate(6,4), new Coordinate(5,5));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(5,5), new Coordinate(4,4));
	        printBoard(board);
	        System.out.println();
	   }
	   
	   public final static void testPawn() 
	   {
	        Board board = new Board(); 
	        board.initializeBoard();
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(1,0), new Coordinate(3,0));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(3,0), new Coordinate(4,0));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(1,1), new Coordinate(2,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(2,1), new Coordinate(3,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(3,1), new Coordinate(4,1));
	        printBoard(board);
	        System.out.println();
	   }
	   
	   public final static void testKnight() 
	   {
	        Board board = new Board(); 
	        board.initializeBoard();
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(1,4), new Coordinate(2,4));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(6,1), new Coordinate(5,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(2,2), new Coordinate(1,4));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(2,2), new Coordinate(3,0));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(3,0), new Coordinate(5,1));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(5,1), new Coordinate(6,3));
	        printBoard(board);
	        System.out.println();
	        board.makeMove(new Coordinate(6,3), new Coordinate(5,1));
	        printBoard(board);
	        System.out.println();
	   }
	   public final static void testCheck() {
	        Board board = new Board(); 
	        board.initializeBoard();
	        printBoard(board);
	        System.out.println();
	        System.out.println(board.makeMove(new Coordinate(7,3), new Coordinate(7,6)));
	        System.out.println(board.makeMove(new Coordinate(3,7), new Coordinate(4,7)));
	        System.out.println();
	        printBoard(board);
	        System.out.println(board.makeMove(new Coordinate(7,6), new Coordinate(7,7)));
	        printBoard(board);
	        System.out.println();
	   }; 
	   
	   public final static void testCheckMate() {
	        Board board = new Board(); 
	        board.initializeBoard();
	        printBoard(board);
	        System.out.println();
	        System.out.println(board.makeMove(new Coordinate(7,3), new Coordinate(7,7)));
	        printBoard(board);
	        System.out.println();
	   }; 
	   
	    public static void main(String[] args) {
//	    			testRook();
//	    			testBishop();
	    			//testPawn();
	    			//testKnight();
	    			testCheck();
				//testCheckMate();
	        }
}
